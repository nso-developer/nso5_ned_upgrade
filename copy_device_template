#!/usr/bin/env python3

import argparse
import contextlib
from functools import wraps
import sys


try:
    import _ncs
    import ncs
    import ncs.experimental
    import ncs.maapi as maapi
except ModuleNotFoundError:
    print("ERROR: NSO Python modules not found.")
    print("You probably haven't sourced the ncsrc file.")
    print("Doing so will set/update the PYTHONPATH environment variable to the NSO modules.")
    sys.exit(1)


"""
Copying of the device template is easier than other trees. All leafs are
strings to allow for variables.
"""


#############
# Globals
#############
debug = False


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--from-ned-id', help="From ned-id", required=False)
    parser.add_argument('-t', '--to-ned-id', help="To ned-id", required=False)
    parser.add_argument('--name', help="Template name", required=False)
    parser.add_argument('-c', '--commit', action="store_true", help="Commit the copied templates.", required=False)
    parser.add_argument('-o', '--overwrite', action="store_true", help="Overwrite if destination ned-id already exists.", required=False)
    parser.add_argument('-d', '--debug', action="store_true", help="Enable debug output. Useful to find the path causing an unhandled exception", required=False)
    parser.add_argument('-l', '--list', action="store_true", help="List templates.", required=False)
    return parser.parse_args()


def _maagic_copy_wrapper(fn):
    """Wrapper for the maagic_copy function, changes input maagic node and tracks recursion depth

    The first argument to the maagic_copy function (source maagic node) will be replaced by
    a maagic node referring to the same node in the data tree, but with a nested transaction
    using a FLAG_NO_DEFAULTS MAAPI flag. This is only done for nodes backed by a transaction.

    The underlying MAAPI transaction of the destination maagic node is also temporarily modified
    to postpone when statement evaluations. The original state is restored before exiting.
    """

    @wraps(fn)
    def wrapper(a, b, _is_first=True):
        if _is_first:
            # When maagic_copy is on the first level of recursion, set MAAPI flag to allow us
            # explicit default value detection with a `C_DEFAULT` value.
            # This is done by:
            #  1. starting a nested transaction using a's maagic object transaction,
            #  2. setting the MAAPI flag in nested transaction,
            #  3. replacing a with a new maagic object backed by the nested transaction

            # Set delayed-when for the destination transaction. The original
            # value is restored after maagic-copy is done.
            try:
                dst_trans = ncs.maagic.get_trans(b)
                orig_delayed_when = dst_trans.set_delayed_when(1)
            except ncs.maagic.BackendError:
                orig_delayed_when = None

            with contextlib.ExitStack() as stack:
                try:
                    src_trans = ncs.maagic.get_trans(a)
                    src_tt = stack.enter_context(src_trans.start_trans_in_trans(ncs.READ))
                    src_tt.set_flags(_ncs.maapi.FLAG_NO_DEFAULTS)
                    a_tt = ncs.maagic.get_node(src_tt, a._path)
                except ncs.maagic.BackendError:
                    a_tt = a
                fn(a_tt, b, _is_first=True)

            if orig_delayed_when is not None:
                dst_trans.set_delayed_when(orig_delayed_when)
        else:
            fn(a, b, _is_first=False)
    return wrapper


def get_child(node, name):
    if node is None:
        return None
    try:
        name = name.replace('-', '_').replace('.', '_')
        return node._children[name]
    except KeyError as e:
#        print("KALLE Exception:", type(e), e.args)
        # Maagic requires some reserved names to have prefix included.
        try:
            pfx = _ncs.ns2prefix(node._cs_node.ns())
            return node._children[pfx + '__' + name]
        except KeyError as e:
            for n in node._children.children:
                pass
                #print(n, flush=True)
    return None


def elem_exists(node, element):
    try:
        return node.exists(element)
    except _ncs.error.Error:
        return False

def safe_create(node, element):
    try:
        return node.create(element)
    except _ncs.error.Error:
        return None

def safe_delete(node, element):
    try:
        del node[element]
    except _ncs.error.Error:
        pass


@_maagic_copy_wrapper
def maagic_copy_template(a, b, errored_node=None, _is_first=True):
#  a is the source node and always has a valid value.
#  b is the destination node and when having a value it should have the
#    same device config path as a.
# When b is None there is not existing destination node and this allows the
# script to report existing nodes (leafs etc.) that can not be copied.
#
    if debug:
        print("--> a={}  b={}".format(a._path, b))

    if isinstance(a, ncs.maagic.NonEmptyLeaf):
        if a.exists():
            if b is not None and isinstance(b, ncs.maagic.NonEmptyLeaf):
                if debug:
                    print("Copying leaf", b._path)
                b.set_value(a.get_value())
            else:
                print("ERROR: Can't copy leaf", a._path, flush=True)

    elif isinstance(a, ncs.maagic.EmptyLeaf):
        if a.exists():
            if b is not None and isinstance(b, ncs.maagic.EmptyLeaf):
                if debug:
                    print("Creating empty leaf", b._path)
                b.create()
            else:
                print("ERROR: Can't create empty leaf", a._path, flush=True)

    elif isinstance(a, ncs.maagic.LeafList):
        if b is not None:
            if isinstance(b, ncs.maagic.LeafList):
                if b.exists():
                    b.delete()
                for c in a:
                    if isinstance(c, _ncs.HKeypathRef): # pylint: disable=no-member#
                        b.create(path_to_xpath(str(c)))
                    else:
                        b.set_value(a.as_list())
                        break
            else:
                print("ERROR: Can't copy leaf-list. Destination is not a leaf-list", a._path)
        else:
            print("ERROR: Can't copy leaf-list. Destination does not exist", a._path)

    elif isinstance(a, ncs.maagic.Container):
        if isinstance(a, ncs.maagic.PresenceContainer):
            if a.exists():
                if b is not None:
                    # Handle change from presence container -> container
                    if isinstance(b, ncs.maagic.PresenceContainer):
                        b.create()
                else:
                    # TODO: Need a way to print the path to the container in b
                    print("ERROR: Can't copy presence container", a._path, flush=True)
                    return # No need to iterate members when a presence container can't be created
            else:
                return # No need to iterate members for non-existing containers
        elif b is not None and isinstance(b, ncs.maagic.PresenceContainer):
            # Handle change from container --> presence container
            b.create()


        # Iterate over members in Contrainer/ListElement
        for src_node in a._children.get_children():
            if src_node._cs_node.is_key():
                continue # Skip key leafs (Only valid for ListElement)
            d = None
            if isinstance(b, ncs.maagic.Container):
                d = get_child(b, str(src_node))
            maagic_copy_template(src_node, d, _is_first=False)

    elif isinstance(a, ncs.maagic.List):
        key_names_b = None
        key_names_a = [_ncs.hash2str(h) for h in a._cs_node.info().keys() or []]
        if b is not None:
            key_names_b = [_ncs.hash2str(h) for h in b._cs_node.info().keys() or []]
        for src_le in a:
            dst_le = None
            if key_names_b is not None:
                if len(key_names_b) != len(key_names_a):
                    print("ERROR: Can't create list element. Number of leafs in key has changed.", a._path)
                    # No need to interate further. If list-element can't be
                    # created, downstream leafs are affected as well.
                else:
                    keys = [src_le[k] for k in key_names_b]
                    dst_le = b.create(*keys)
                    maagic_copy_template(src_le, dst_le, _is_first=False)
            else:
                ks = [src_le[k] for k in key_names_a]
                print("ERROR: Can't copy list element {}{{{}}}".format(a._path, " ".join(ks)), flush=True)

def get_device_templates(t, nedid):
    result = {}
    q = ncs.experimental.Query(t, '/devices/template/ned-id', None,
            ['../name', 'id'],
            result_as=ncs.QUERY_STRING)
    for r in q:
        name = r[0]
        nid = r[1].split(':')[1]
        if name in result:
            result[name].append(nid)
        else:
            result[name] = [nid]
    return result

def get_ncs_version(t):
    root = ncs.maagic.get_root(t)
    return root.ncs_state.version

def copy_template(t, name, from_ned_id, to_ned_id, overwrite=False):
    root = ncs.maagic.get_root(t)
    if not elem_exists(root.devices.template, name):
        print("ERROR: Template {} not found".format(name))
        sys.exit(1)
    tmpl = root.devices.template[name]

    tmpl.ned_id._populate()
    if not elem_exists(tmpl.ned_id, from_ned_id):
        print("ERROR: Template {} nedid {} not found".format(name, from_ned_id))
        sys.exit(1)
    fr = tmpl.ned_id[from_ned_id]

    if elem_exists(tmpl.ned_id, to_ned_id) and overwrite:
        safe_delete(tmpl.ned_id, to_ned_id)

    to = safe_create(tmpl.ned_id, to_ned_id)
    if to is None:
        print("ERROR: Can't create Template {} nedid {}".format(name, to_ned_id))
        sys.exit(1)

    maagic_copy_template(fr.config, to.config)


if __name__ == '__main__':
    args = parseArgs()

    debug = args.debug

    if not args.list and args.name:
        if not args.from_ned_id or not args.to_ned_id:
            print("From and to ned-ids must be specified")
            sys.exit(1)
        if args.from_ned_id == args.to_ned_id:
            print("From and to ned-ids are equal")
            sys.exit(1)

    try:
        with ncs.maapi.Maapi() as m:
            with ncs.maapi.Session(m, 'admin', 'system'):
                with m.start_write_trans() as t:
                    if get_ncs_version(t).split('.')[0] != '5':
                        print("copy_template is only supported on NSO 5.x")
                        sys.exit(1)
                    if args.list:
                        all_tmpls = get_device_templates(t, args.from_ned_id)
                        for tmpl, nids in all_tmpls.items():
                            while nids:
                                print("{:30} {}".format(tmpl, nids[0]))
                                tmpl = "" # Only print template name on first row
                                del nids[0]
                        sys.exit(0)
                    if args.name:
                        print("Copying device template {}: {} --> {}{}".format(args.name, args.from_ned_id, args.to_ned_id, "" if args.commit else "  (DRY-RUN)"), flush=True)
                        copy_template(t, args.name, args.from_ned_id, args.to_ned_id, args.overwrite)
                    else:
                        all_tmpls = get_device_templates(t, args.from_ned_id)
                        from_tmpls = list(filter(lambda x: x[1]==args.from_ned_id, all_tmpls))
                        for tmpl in from_tmpls:
                            print("Copying device template {}: {} --> {}{}".format(tmpl[0], args.from_ned_id, args.to_ned_id, "" if args.commit else "  (DRY-RUN)"), flush=True)
                            copy_template(t, tmpl[0], args.from_ned_id, args.to_ned_id, args.overwrite)
                    if args.commit:
                        t.apply()
    except KeyboardInterrupt:
        print()
    except BrokenPipeError:
        print()
