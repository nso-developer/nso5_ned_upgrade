# NSO 5.x NED Upgrade
## Overview
The background of the scripts is to help the administrator of an NSO system to
upgrade NEDs and it should be compatible with existing package management
processes in use (with minor adjustments).

In NSO version prior to 5.x an upgrade was "just" to replace a package and issue
a simple reload command. As of 5.x and the introduction of CDM, Common Data Model,
the effort became more noticeble. You not only may need to know if you where
a minor or major upgrade, you also need to keep two versions of a NED loaded
at the same time and need to migrate devices from one version to another. on top
of that there might be scenarios where a migration of a device might fail.

The concept of the NED upgrade script is setup to be simple:
 - Only the latest version of a NED type should be installed and in use at a
   particular moment.

Also device template was affected by the CDM design change. Now they manually
need to copied to the new NED-id as not builtin functionally yat exists for this.
The script copy-device-template is intended to help with this.

## Commands
A number of commands are provided by the script to minimized the need for
opening an additional NSO CLI session in parallel:
 - Show packages (loaded, installed and installable)
 - Install a package from installable to installed.
 - Deinstall a package from installed.
 - Reload packages.

```
root@n1:~nso-project# ./nedupgrade-link -h
usage: nedupgrade-link [-h] [--rdir RDIR] [--pdir PDIR] [-v] [-d] [-p] [-n] [--nc] [--exec] [-o OVERRIDE]
                       [--pkg PKG] [--vers VERS] [--force] {upgrade,list,install,deinstall,reload}

positional arguments:
  {upgrade,list,install,deinstall,reload}
                        Command to be excuted.

optional arguments:
  -h, --help            show this help message and exit
  --rdir RDIR           Run directory. Default is /var/opt/ncs/packages
  --pdir PDIR           Packages directory. Default is /opt/ncs/packages
  -v, --version         Show version.
  -d, --debug           Print debug information
  -p, --pause           Pause between steps.
  -n, --neds            Show only NEDs.
  --nc                  No colored output.
  --exec                Run checks but do not contiune and do the upgrade.
  -o OVERRIDE, --override OVERRIDE
                        Override NED version to use.
  --pkg PKG             Package to install or deinstall.
  --vers VERS           Package version to install or deinstall.
  --force               Do a forced packages reload..
```
## Upgrade
The main function of the script is the 'upgrade' command:
 - NEDs are installed in the packages directory, conceptually named 'installed',
   normally '/var/opt/ncs/packages'. Can be changed with option --rdir.
 - NEDs taken into account is located in the local packages repository
   directory, conceptually named 'installable', normally '/opt/ncs/packages'.
   Can be changed with option --pdir.
 - NEDs are installed as symlinks in 'installed' pointing to the 'installable'
   directory.
 - It calculates what to do from which NEDs are available in 'installable',
   installed and loaded, which devices uses which ned-ids etc.
 - The script only takes loaded NEDs that are in use by devices into account
   when deciding what to do.

Running the 'upgrade' commands basically follows the steps:
 1. Browse 'installable', 'installed' and 'loaded' to find which the newest NEDs.
 2. Calculate what to do from the information collected in 1.
 3. Do the calculated steps:
   1. Uninstall NEDs with minor version change.
   2. Install the newest version of affected NEDs.
   3. Reload packages without force to load any new NEDs.
   4. Migrate devices to newest NEDs (major version change only)
   5. Uninstall NEDs migrated from (major version change only)
   6. Reload packages without force to determine the YANG models to be unloaded
      are related to the uninstalled NEDs by the upgrade.

The script also does a lot of additional checks to guarantee consistency among
packages before executing any commands, e.g. packages reload force. This to provide
security from unwanted changes.
By default the upgrade command runs in dry-run mode to show what its intentions
are before executing them. Execution of the intentions are explicitly requested
by adding the option --exec.



## Example of running the list command
```
HA status: none
Read-only mode: False

Installable packages:
+--------------------+-------------+------------+----------+--------------------+------+
| pkg-name           | pkg-version | pkg-format | pkg-type | ned-id             | skip |
+--------------------+-------------+------------+----------+--------------------+------+
| gen_ned-gen-1.0    | 1.0         | dir        | ned      | gen_ned-gen-1.0    |      |
| cisco-ios-cli-6.66 | 6.66.1      | dir        | ned      | cisco-ios-cli-6.66 |      |
| cisco-ios-cli-6.64 | 6.64.4      | dir        | ned      | cisco-ios-cli-6.64 |      |
| cisco-ios-cli-6.60 | 6.60        | dir        | ned      | cisco-ios-cli-6.60 |      |
| gen_ned-gen-1.1    | 1.1         | dir        | ned      | gen_ned-gen-1.1    |      |
| cisco-ios-cli-6.65 | 6.65        | dir        | ned      | cisco-ios-cli-6.65 |      |
| cisco-ios-cli-6.60 | 6.60.1      | dir        | ned      | cisco-ios-cli-6.60 |      |
| tailf-hcc          | 4.5.0       | tgz        |          |                    |      |
+--------------------+-------------+------------+----------+--------------------+------+

Installed packages:
+--------------------+-------------+------------+----------+--------------------+------+
| pkg-name           | pkg-version | pkg-format | pkg-type | ned-id             | skip |
+--------------------+-------------+------------+----------+--------------------+------+
| cisco-ios-cli-6.60 | 6.60        | dir        | ned      | cisco-ios-cli-6.60 |      |
| cisco-ios-cli-6.64 | 6.64.4      | dir        | ned      | cisco-ios-cli-6.64 |      |
| gen_ned-gen-1.1    | 1.1         | dir        | ned      | gen_ned-gen-1.1    |      |
| tailf-hcc          | 4.5.0       | tgz        |          |                    |      |
+--------------------+-------------+------------+----------+--------------------+------+

Loaded packages:
+--------------------+-------------+----------+--------------------+
| pkg-name           | pkg-version | pkg-type | ned-id             |
+--------------------+-------------+----------+--------------------+
| cisco-ios-cli-6.60 | 6.60        | ned      | cisco-ios-cli-6.60 |
| gen_ned-gen-1.1    | 1.1         | ned      | gen_ned-gen-1.1    |
| tailf-hcc          | 4.5.0       |          |                    |
+--------------------+-------------+----------+--------------------+

NED ID(s) in use by devices:
+--------------------+-----------------+
| cisco-ios-cli-6.60 | gen_ned-gen-1.1 |
+--------------------+-----------------+
| dev1               | dev5            |
| dev2               |                 |
| dev3               |                 |
| dev4               |                 |
+--------------------+-----------------+
```


## Example of running the upgrade command in dry-run mode

```
root@n1:~nso-project# ./nedupgrade-link upgrade
HA status: none
Read-only mode: False

NEDs to be replaced:
 -

NEDs to be installed:
cisco-ios-cli-6.66 version 6.66.1

Devices to migrate:
{'cisco-ios-cli-6.66': ['dev1', 'dev2', 'dev3', 'dev4']}

NEDs to be unloaded:
['cisco-ios-cli-6.60']
```

## Example of running the upgrade command with --exec option

```
root@n1:~/nso-project# ./nedupgrade-link upgrade --exec
HA status: none
Read-only mode: False

NEDs to be replaced:
 -

NEDs to be installed:
cisco-ios-cli-6.66 version 6.66.1

Devices to migrate:
{'cisco-ios-cli-6.66': ['dev1', 'dev2', 'dev3', 'dev4']}

NEDs to be unloaded:
['cisco-ios-cli-6.60']

Installing package: symlink /opt/ncs/packages/cisco-ios-cli-6.66-lite --> /var/opt/ncs/packages/cisco-ios-cli-6.66-lite

Reloading packages - Loading new NED(s):
+--------------------+--------+------+
| pkg-name           | result | info |
+--------------------+--------+------+
| cisco-ios-cli-6.60 | True   |      |
| cisco-ios-cli-6.64 | True   |      |
| cisco-ios-cli-6.66 | True   |      |
| gen_ned-gen-1.1    | True   |      |
| tailf-hcc          | True   |      |
+--------------------+--------+------+
Schema reloaded

Migrating devices: ['dev1', 'dev2', 'dev3', 'dev4']  -->  cisco-ios-cli-6.66
*** Migrating devices ***
old None
new cisco-ios-cli-6.66
dev1 True None
dev2 True None
dev3 True None
dev4 False Device dev4 is southbound locked
*** Done migrating devices ***
PARTIAL SUCCESS Some devices where not upgraded ['dev4'].
Will will migrate ['dev4'] using no-networking
*** Migrating devices ***
old None
new cisco-ios-cli-6.66
dev4 True None
*** Done migrating devices ***
SUCCESS All devices upgraded to new NED.

Deinstalling old NED(s)
Removing symlink /var/opt/ncs/packages/cisco-ios-cli-6.60 to prevent loading

Reloading packages with force=false to determine which modules to be unloaded.
Continuing with a force=true packages reload...
+--------------------+--------+------+
| pkg-name           | result | info |
+--------------------+--------+------+
| cisco-ios-cli-6.64 | True   |      |
| cisco-ios-cli-6.66 | True   |      |
| gen_ned-gen-1.1    | True   |      |
| tailf-hcc          | True   |      |
+--------------------+--------+------+
```
